package petshop;

public abstract class Animal implements AnimalIfc{
	
	String nome;
	int idade;
	public TipoRaca raca;
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public int getIdade() {
		return idade;
	}
	public void setIdade(int idade) {
		this.idade = idade;
	}
	
	public void dormir(){
	
		System.out.println("zzzZZZzzz!");
	}
	
	
	public abstract void fazBarulho();
	public abstract void procuraComida();

}
