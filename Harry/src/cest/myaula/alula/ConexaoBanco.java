package cest.myaula.alula;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
public class ConexaoBanco {
	public Connection getConnection(String user, String pwd) 
			throws ClassNotFoundException, SQLException{
		
		
		String url = "jdbc:postgresql://localhost:5432/dbs";
		
		Class classobj =
		 Class.forName("org.postgresql.Driver");
		Connection con = (Connection)
					DriverManager.getConnection(url, user, pwd);
		return con;
	}

}
