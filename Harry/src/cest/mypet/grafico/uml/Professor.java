package cest.mypet.grafico.uml;

public class Professor {
	public int regime;
	public String titulacao;
	
	public int getregime() {
		return regime;
	}
	public void setregime(int regime) {
		this.regime = regime;
	}
	public String gettitulacao() {
		return titulacao;
	}
	public void settitulacao(String titulacao) {
		this.titulacao = titulacao;
	}
	

}
