package cest.mypet.cadastro;

public class Vendedor extends Funcionario{
	private double comissao = 0;
	
	public void venda(double valorVenda) {
		this.comissao += (valorVenda * 0.10);
		
	}
	public double getcomissao() {
		return comissao;
	}
	@Override
	public double getSalarioTotal() {
		return getSalarioBase() + comissao * 2;
	}
	

}
