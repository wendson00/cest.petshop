package cest.mypet.cadastro;

import java.util.Date;

public abstract class Funcionario {
	private String matricula;
	private double SalarioBase;
	private Date staContrato;
	
	public String getMatricula() {
		return matricula;
	}


	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}


	public double getSalarioBase() {
		return SalarioBase;
	}


	public void setSalarioBase(double salarioBase) {
		SalarioBase = salarioBase;
	}


	public Date getStaContrato() {
		return staContrato;
	}


	public void setStaContrato(Date staContrato) {
		this.staContrato = staContrato;
	}


	public abstract double getSalarioTotal();

}
