package cest.mypet.cadastro;

import java.util.ArrayList;

public class CalculaSalario {
	public static void main(String[] args) {
		Gerente g = new Gerente();
		Vendedor v = new Vendedor();
		GuardaNoturno gn = new GuardaNoturno();
		ArrayList<Funcionario> funs = new ArrayList<Funcionario>();
		
		funs.add(g);
		funs.add(v);
		funs.add(gn);
		g.setSalarioBase(1000);
		v.setSalarioBase(100);
		gn.setSalarioBase(800);
		
		v.venda(100);
		double totalFolha = 0;
		for (Funcionario f : funs) {
			System.out.println("SB:" + f.getSalarioBase());
			totalFolha += f.getSalarioTotal();
		}
		System.out.println("Base + comissao: " + totalFolha);
	}

}
