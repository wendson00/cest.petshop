package cest.mypet.cadastro.loc.teste;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import cest.mypet.cadastro.loc.Cidade;
import cest.mypet.cadastro.loc.UF;

class CidadeTest {

	@Test
	void test() {
		Cidade cidTeste = new Cidade();
		cidTeste.nome = "Sao Luis";
		
		UF ufTeste = new UF();
		ufTeste.cod = "MA";
		ufTeste.descricao = "Maranhao";
		
		assertEquals("Sao Luis", cidTeste.nome);
		
		assertEquals("MA", ufTeste.cod);
		assertEquals("Maranhao", ufTeste.descricao);
				
		
	}

}
