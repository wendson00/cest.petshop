package cest.mypet.cadastro.loc.teste;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import cest.mypet.cadastro.TipoAnimal;

class TipoAnimalTest {

	@Test
	void test() {
		TipoAnimal tipoteste = new TipoAnimal();
		tipoteste.cod = 012;
		tipoteste.descricao = "Dogui";
		
		assertEquals(012, tipoteste.cod);
		assertEquals("Dogui", tipoteste.descricao);
		
		
	}

}
