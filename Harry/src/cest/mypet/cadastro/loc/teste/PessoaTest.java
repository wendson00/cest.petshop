package cest.mypet.cadastro.loc.teste;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import cest.mypet.cadastro.Pessoa;
import cest.mypet.cadastro.loc.Cidade;

class PessoaTest {

	@Test
	void test() {
		Pessoa pesteste = new Pessoa();
		pesteste.nome = "Harry";
		pesteste.idade = 21;
		
		pesteste.cidade = new Cidade ();
		pesteste.cidade.nome = "Sao Luis";
				
		assertEquals("Harry", pesteste.nome);
		assertEquals(21, pesteste.idade);
		
		assertEquals("Sao Luis", pesteste.cidade);
	}

}
