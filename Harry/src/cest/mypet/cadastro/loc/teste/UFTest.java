package cest.mypet.cadastro.loc.teste;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import cest.mypet.cadastro.loc.UF;

class UFTest {

	@Test
	void test() {
		UF ufTeste = new UF();
		ufTeste.setCod ("MA");
		ufTeste.setDescricao ("Maranhao");
		
		assertEquals("MA", ufTeste.getCod());
		assertEquals("Maranhao", ufTeste.getDescricao());
		
		
		
	}

}
