package cest.mypet.cadastro.loc.teste;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import cest.mypet.cadastro.Animal;
import cest.mypet.cadastro.TipoAnimal;

class AnimalTest {

	@Test
	void test() {
		Animal AniTeste = new Animal ();
		AniTeste.nome = "Mauricio";
		AniTeste.idade = 53;
		
		AniTeste.tipo = new TipoAnimal();
		AniTeste.tipo.cod = 012;
		AniTeste.tipo.descricao = "Dogui";
		
		assertEquals("Mauricio", AniTeste.nome );
		assertEquals(53 , AniTeste.idade);
		
		assertEquals(012, AniTeste.tipo.cod);
		assertEquals("Dogui", AniTeste.tipo.descricao);
		
		
		
				
		
	}

}
